package com.milewski.ex02.core;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.milewski.ex02.JChannels;
import com.milewski.ex02.gui.GUI;
import org.jgroups.*;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public class Chat {
    public static final String INFO_CHANNEL_NAME = "ChatManagement768624";

    private final List<GUI> guis = Lists.newArrayList();

    private final String nickname;
    private final JChannel infoChannel;

    private final Map<String, Channel> channels = Maps.newHashMap();
    private final Map<Address, String> addresses = Maps.newHashMap();

    public Chat(String nickname) throws Exception {
        this.nickname = nickname;
        infoChannel = JChannels.newJChannel();
    }

    public void init() throws Exception {
        infoChannel.connect(INFO_CHANNEL_NAME);
        infoChannel.setReceiver(new Receiver());
        infoChannel.getState(null, 10000);
    }

    public void registerGui(GUI gui) {
        guis.add(gui);
    }

    public void joinTo(String address) throws Exception {
        Channel channel;
        if (channels.containsKey(address)) {
            channel = channels.get(address);
        } else {
            channel = Channel.create(address);
            for (GUI gui : guis) {
                channel.registerGui(gui);
            }
            channels.put(address, channel);
        }
        try {
            channel.join();
        } catch (IllegalArgumentException e) {
            channels.remove(address);
        }
        sendJoinMessage(address);
    }


    public void cleanUp() throws Exception {
        for (Map.Entry<String, Channel> entry : channels.entrySet()) {
            entry.getValue().leave();
            entry.getValue().cleanUp();
            sendLeaveMessage(entry.getKey());
        }
        this.infoChannel.close();
    }

    private void sendJoinMessage(String address) throws Exception {
        ChatAction chatAction = ChatAction.newBuilder()
                .setAction(ChatAction.ActionType.JOIN)
                .setChannel(address)
                .setNickname(nickname)
                .build();
        Message msg = new Message(null, null, chatAction);
        infoChannel.send(msg);
    }

    private void sendLeaveMessage(String address) throws Exception {
        ChatAction chatAction = ChatAction.newBuilder()
                .setAction(ChatAction.ActionType.LEAVE)
                .setChannel(address)
                .setNickname(nickname)
                .build();
        Message msg = new Message(null, null, chatAction);
        infoChannel.send(msg);
    }

    private void analyze(Address src, ChatAction action) throws Exception {
        ChatAction.ActionType type = action.getAction();
        switch (type) {
            case JOIN:
                onJoinAction(src, action);
                break;
            case LEAVE:
                onLeaveAction(action);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    private void onJoinAction(ChatAction action) throws Exception {
        if (action.getAction() != ChatAction.ActionType.JOIN) {
            throw new IllegalStateException("Illegal Action");
        }

        String channelAddress = action.getChannel();
        Channel channel;
        if (channels.containsKey(channelAddress)) {
            channel = channels.get(channelAddress);
        } else {
            channel = Channel.create(channelAddress);
            for (GUI gui : guis) {
                channel.registerGui(gui);
            }
            channels.put(channelAddress, channel);
        }
        channel.addUser(action.getNickname());
    }

    private void onJoinAction(Address src, ChatAction action) throws Exception {
        onJoinAction(action);
        if (!addresses.containsKey(src)) {
            addresses.put(src, action.getNickname());
        }
    }

    private void onLeaveAction(ChatAction action) {
        if (action.getAction() != ChatAction.ActionType.LEAVE) {
            throw new IllegalStateException("Illegal Action");
        }
        String channelAddress = action.getChannel();
        if (channels.containsKey(channelAddress)) {
            Channel channel = channels.get(channelAddress);
            channel.removeUser(action.getNickname());
            if (channel.hasNoUsers()) {
                channels.remove(channelAddress);
            }
        }
    }

    public List<Channel> getChannels() {
        return ImmutableList.copyOf(channels.values());
    }

    public void sendMessage(Channel channel, String msg) throws Exception {
        if (channels.containsValue(channel)) {
            channel.send(msg);
        } else {
            throw new IllegalArgumentException("Channel does not exist");
        }
    }

    public void leave(Channel channel) throws Exception {
        if (channels.containsValue(channel)) {
            channel.leave();
            sendLeaveMessage(channel.getAddress());
        } else {
            throw new IllegalArgumentException("Channel does not exist");
        }
    }

    public List<Channel> getJoinedChannels() {
        return ImmutableList.copyOf(Iterables.filter(channels.values(), new Predicate<Channel>() {
            @Override
            public boolean apply(Channel input) {
                return input.isJoined();
            }
        }));
    }

    private class Receiver extends ReceiverAdapter {
        @Override
        public void receive(Message msg) {
            Object object = msg.getObject();
            if (object instanceof ChatAction) {
                ChatAction action = (ChatAction) object;
                try {
                    analyze(msg.getSrc(), action);
                } catch (Exception e) {
                    for (GUI gui : guis) {
                        gui.onException(e);
                    }
                }
            }
        }

        @Override
        public void viewAccepted(View view) {
            for (Address address : addresses.keySet()) {
                if (!view.getMembers().contains(address)) {
                    for (Channel channel : channels.values()) {
                        channel.removeUser(addresses.get(address));
                        if (channel.hasNoUsers()) {
                            channels.remove(channel.getAddress());
                        }
                    }
                }
            }
        }

        @Override
        public void getState(OutputStream output) throws Exception {
            ChatOperationProtos.ChatState.Builder builder = ChatOperationProtos.ChatState
                    .newBuilder();

            for (Channel channel : channels.values()) {
                final Channel c = channel;
                builder.addAllState(Lists.transform(channel.getUsers(), new Function<String, ChatAction>() {
                    @Override
                    public ChatAction apply(String nickname) {
                        return ChatAction.newBuilder()
                                .setAction(ChatAction.ActionType.JOIN)
                                .setChannel(c.getAddress())
                                .setNickname(nickname)
                                .build();
                    }
                }));
            }
            output.write(builder.build().toByteArray());
        }

        @Override
        public void setState(InputStream input) throws Exception {
            ChatOperationProtos.ChatState chatState = ChatOperationProtos.ChatState.parseFrom(input);
            for (ChatAction chatAction : chatState.getStateList()) {
                if (chatAction.getAction() == ChatAction.ActionType.JOIN) {
                    onJoinAction(chatAction);
                } else if (chatAction.getAction() == ChatAction.ActionType.LEAVE) {
                    onLeaveAction(chatAction);
                }
            }
        }
    }
}
