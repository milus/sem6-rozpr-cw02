package com.milewski.ex02.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.milewski.ex02.JChannels;
import com.milewski.ex02.gui.GUI;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

public class Channel {
    private final String address;
    private final Set<String> users = Sets.newHashSet();

    private boolean listen = false;

    private final JChannel channel;

    private final List<GUI> guis = Lists.newArrayList();

    private Channel(final String address) throws Exception {
        this.address = address;
        this.channel = JChannels.newJChannel(address);
        this.channel.setReceiver(new ReceiverAdapter() {
            @Override
            public void receive(Message msg) {
                Object object = msg.getObject();
                if (object instanceof ChatOperationProtos.ChatMessage) {
                    if (listen) {
                        if (!channel.getAddress().equals(msg.getSrc())) {
                            for (GUI gui : guis) {
                                gui.onNewMessage(address, ((ChatOperationProtos.ChatMessage) object).getMessage());
                            }
                        }
                    }
                }
            }
        });
    }

    // CHANNEL MANAGEMENT
    public static Channel create(String address) throws Exception {
        return new Channel(address);
    }

    public void join() throws Exception {
        listen = true;
        this.channel.connect(address);
    }

    public void leave() {
        listen = false;
        this.channel.disconnect();
    }

    public boolean isJoined() {
        return listen;
    }

    public void cleanUp() {
        this.channel.close();
    }

    public void registerGui(GUI gui) {
        guis.add(checkNotNull(gui));
    }

    // PUBLIC GETTERS
    public String getAddress() {
        return address;
    }

    // USER MANAGEMENT
    public List<String> getUsers() {
        return ImmutableList.copyOf(users);
    }

    public void addUser(String nickname) {
        users.add(checkNotNull(nickname));
    }

    public void removeUser(String nickname) {
        if (users.contains(nickname)) {
            users.remove(nickname);
        }
    }

    public boolean hasNoUsers() {
        return users.isEmpty();
    }

    // MESSAGING
    public void send(String msg) throws Exception {
        ChatOperationProtos.ChatMessage chatMessage = ChatOperationProtos.ChatMessage.newBuilder()
                .setMessage(msg)
                .build();
        Message message = new Message(null, null, chatMessage);
        this.channel.send(message);
    }
}
