package com.milewski.ex02.gui;

public interface GUI {
    void onException(Exception e);

    void onJoinAction(String nickname, String channel);

    void onNewMessage(String channelAddress, String msg);
}
