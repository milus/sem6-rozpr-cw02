package com.milewski.ex02.gui;

import com.milewski.ex02.core.Channel;
import com.milewski.ex02.core.Chat;
import org.apache.commons.lang3.StringUtils;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.strip;

public class TerminalGUI implements GUI {
    private final Scanner scanner = new Scanner(System.in);
    private Chat chat;

    public static void main(String[] args) {
        new TerminalGUI().start();
    }

    public void start() {
        System.out.println("Type in your nickname");
        String nickname = loadNickname();
        try {
            initializeChat(nickname);
            mainMenu();
        } catch (Exception e) {
            onException(e);
        }
    }

    private void mainMenu() throws Exception {
        System.out.println("What do you want to do now? Please insert number or \"\\q\" if you want to exit from chat");
        System.out.println("1. Join to channel (or create if do not exist)");
        System.out.println("2. List channels and users");
        System.out.println("3. Send message in channel");
        System.out.println("4. Leave channel");

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (equalsIgnoreCase(strip(command), "\\q")) {
                exitFromChat();
            } else if (equalsIgnoreCase(strip(command), "1")) {
                joinOrCreateChannel();
            } else if (equalsIgnoreCase(strip(command), "2")) {
                listChannelsAndUsers();
            } else if (equalsIgnoreCase(strip(command), "3")) {
                sendMessageInChannel();
            } else if (equalsIgnoreCase(strip(command), "4")) {
                leaveChannel();
            } else {
                System.out.println("Incorrect command. Please try again. Commands: \\q, 1, 2, 3, 4");
            }
            System.out.println("What do you want to do now? Please insert number or \"\\q\" if you want to exit from chat");
            System.out.println("1. Join to channel (or create if do not exist)");
            System.out.println("2. List channels and users");
            System.out.println("3. Send message in channel");
            System.out.println("4. Leave channel");
        }

    }

    // \q
    private void exitFromChat() throws Exception {
        chat.cleanUp();
        System.out.println("Exiting from chat.");
        System.exit(0);
    }

    // 1
    private void joinOrCreateChannel() throws Exception {
        System.out.println("Insert channel's address or \\q to quit");
        if (scanner.hasNext()) {
            String address = scanner.next();
            if (equalsIgnoreCase(strip(address), "\\q")) {
                exitFromChat();
                return;
            }
            try {
                chat.joinTo(address);
            } catch (UnknownHostException | IllegalArgumentException e) {
                System.out.println("Incorrect address. Please try again");
                joinOrCreateChannel();
            }
        }
    }
    // 2
    private void listChannelsAndUsers() {
        List<Channel> channels = chat.getChannels();
        for (Channel channel : channels) {
            System.out.println("Channel " + channel.getAddress());
            for (String user : channel.getUsers()) {
                System.out.println("    User + " + user);
            }
        }
    }
    // 3
    private void sendMessageInChannel() throws Exception {
        List<Channel> channels = chat.getJoinedChannels();
        if (channels.isEmpty()) {
            System.out.println("There's no channels you are connected to.");
            return;
        }
        System.out.println("Choose channel. Insert number:");
        for (int i = 0; i < channels.size(); i++) {
            System.out.println(i + ". " + channels.get(i).getAddress());
        }
        if (scanner.hasNext()) {
            int channelNo = 0;
            try {
                channelNo = Integer.parseInt(scanner.next());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect value.");
                return;
            }
            System.out.println("Please insert your message:");
                if (scanner.hasNext()) {
                    String msg = scanner.next();
                    chat.sendMessage(channels.get(channelNo), msg);
                } else {
                    exitFromChat();
                }

        } else {
            exitFromChat();
        }
    }
    // 4
    private void leaveChannel() throws Exception {
        System.out.println("Choose channel. Insert number:");
        List<Channel> channels = chat.getChannels();
        for (int i = 0; i < channels.size(); i++) {
            System.out.println(i + ". " + channels.get(i).getAddress());
        }
        if (scanner.hasNext()) {
            int channelNo = 0;
            try {
                channelNo = Integer.parseInt(scanner.next());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect value.");
                return;
            }
            chat.leave(channels.get(channelNo));
        } else {
            exitFromChat();
        }
    }

    private String loadNickname() {
        String nickname = scanner.next();
        if (StringUtils.isNotBlank(nickname)) {
            return nickname;
        }

        System.out.println("Your nickname can't be empty. Please try again");
        while (scanner.hasNext()) {
            nickname = scanner.next();
            if (StringUtils.isNotBlank(nickname)) {
                return nickname;
            }
            System.out.println("Your nickname can't be empty. Please try again");
        }
        System.out.println("You don't want to tell me your nickname, so I am going to quit.");
        System.exit(0);
        return null;
    }

    private void initializeChat(String nickname) throws Exception {
        System.out.println("Hello " + nickname);
        chat = new Chat(nickname);
        chat.registerGui(this);
        chat.init();
    }



    @Override
    public void onException(Exception e) {
        System.err.println("Exception");
        try {
            chat.cleanUp();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        e.printStackTrace();
        System.exit(1);
    }

    @Override
    public void onJoinAction(String nickname, String channel) {
        System.out.println(nickname + " has joined to " + channel);
    }

    @Override
    public void onNewMessage(String channelAddress, String msg) {
        System.out.println(channelAddress + ": " + msg);
    }
}
